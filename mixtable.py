# Simple Mixtable interface - Copyleft Lena Siess

# TODO
# * Scroll when slider amount exceeds screen real estate (enhancement)
# * Better graphics (enhacement)

import pygame

# SETUP PYGAME
pygame.init()
pygame.mixer.quit()
pygame.font.init()
screen = pygame.display.set_mode([800,480])

# VISUALS SETUP
font = pygame.font.SysFont(None, 25)
LeftBorder = 40      # X-offset of first slider
SliderDistance = 120 # Distance between sliders (mid-point to mid-point)
SliderWidth = 80     # Width of slider-button
SliderHeight = 40    # Height of slider-button
Tolerance = 5        # Pixel space to make sure slider is at 0% or 100%

# ELEMENTS Y POSITION SETUP
VolumeLabelOffset = 20
MaxOffset = VolumeLabelOffset + 50
MinOffset = MaxOffset + 300
MuteOffset = MinOffset + 40
LabelOffset = MuteOffset + 30

# INPUT VARIABLE
AlreadyClicked = False

# SETUP SLIDERS
# A slider object is defined as (name, Y-position on screen, muted)
sliders = []

with open('sliderconfig', 'r') as file:
    for line in file:
        if line != "":
            sliders.append((line[:-1],MinOffset,False)) #strip the newline character from file

# FUNCTIONS
def CheckTrack(mousex, mousey): # Which track was clicked?
    slidernumber = -1
    if mousex >= LeftBorder:
        slidernumber = int((mousex-LeftBorder) / (SliderWidth+(SliderDistance-SliderWidth)))
    if slidernumber != -1 and slidernumber < len(sliders):
        # Check if you clicked between the tracks, or correctly
        if mousex > LeftBorder + (SliderDistance * slidernumber) and mousex < LeftBorder + SliderWidth + (SliderDistance * slidernumber):
            # These two functions return True when user clicked on the element
            if CheckSliders(slidernumber, mousey) or CheckMuteButton(slidernumber, mousey):
                PrintAudioInfo()

def CheckSliders(slidernumber, mousey): # Was the slider clicked?
    global AlreadyClicked
    # Determine what slider was clicked
    if mousey < MinOffset and mousey > MaxOffset:
        sliders[slidernumber] = (sliders[slidernumber][0],mousey,sliders[slidernumber][2])
        DrawScreen()
        AlreadyClicked = True
        return True
    return False

def CheckMuteButton(slidernumber, mousey): # Was the mute button clicked?
    global AlreadyClicked
    if not AlreadyClicked:
        if mousey < MuteOffset + (SliderHeight/2) and mousey > MuteOffset:
            sliders[slidernumber] = (sliders[slidernumber][0],sliders[slidernumber][1], not sliders[slidernumber][2])
            DrawScreen()
            AlreadyClicked = True
            return True
    return False

def DrawScreen():
    screen.fill(pygame.Color(80,0,0))
    for idx, slider in enumerate(sliders):
        # Draw Track Volume
        if slider[2]:
            volume_string = "MUTED"
        else:
            volume_string = "0db / " + "{0:.2f}".format(CalculatePercentageFromYPosition(slider[1])) + "%"
        label = font.render(volume_string, 1, (0,0,0))
        screen.blit(label, ((LeftBorder + (idx*SliderDistance))+(SliderWidth/2)-(label.get_width()/2), VolumeLabelOffset))
        # Draw Slider Button
        pygame.draw.rect(screen, (0,80,0),
                [LeftBorder+(idx*SliderDistance),slider[1]-(SliderHeight/2),SliderWidth,SliderHeight])
        # Draw Mute Button
        if slider[2]:
            pygame.draw.rect(screen, (190,20,40),
                    [LeftBorder+(idx*SliderDistance),MuteOffset,SliderWidth,SliderHeight/2])
        else:
            pygame.draw.rect(screen, (80,80,0),
                    [LeftBorder+(idx*SliderDistance),MuteOffset,SliderWidth,SliderHeight/2])
        # Draw Track Label
        label = font.render(slider[0], 1, (0,0,0))
        screen.blit(label, ((LeftBorder + (idx*SliderDistance))+(SliderWidth/2)-(label.get_width()/2), LabelOffset))

    pygame.display.flip()

def CalculatePercentageFromYPosition(ypos):
    tolerantMaxY = MaxOffset + Tolerance
    tolerantMinY = MinOffset - Tolerance
    percentage = (float((ypos - tolerantMaxY)) / float((tolerantMinY - tolerantMaxY))) * 100
    clamped = max(min(percentage, 100), 0)
    return 100.0 - clamped

def PrintAudioInfo():
    for slider in sliders:
        print ("[" + str(slider[0]) + ", " + str(CalculatePercentageFromYPosition(slider[1])) + ", " + str(slider[2]) + "]")

# MAIN LOOP
running = True

DrawScreen()

while running:
    event = pygame.event.wait()
    if event.type == pygame.QUIT:
            running = False

    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
            running = False

    if pygame.mouse.get_pressed() == (True, False, False):
        mousex, mousey = pygame.mouse.get_pos()
        CheckTrack(mousex,mousey)
        #CheckSliders()
        #CheckMuteButton()
    else:
        AlreadyClicked = False

pygame.quit()
